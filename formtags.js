function showPassword() {
    let password = document.getElementById('password');
    if (password.type === 'password') {
      password.type = 'text';
    } else {
      password.type = 'password';
    }
  }
  
  document.addEventListener('DOMContentLoaded', (event) => {
    let submitBtn = document.getElementById('submit');
    submitBtn.addEventListener('click', function (event) {
      event.preventDefault();
  
      let firstName = document.getElementById('firstname');
      let lastName = document.getElementById('lastname');
      let password = document.getElementById('password').value;
      let reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
  
      if (firstName.value === "" || firstName.value === null) {
      
       document.getElementById("fname").innerHTML = "Please Enter firstname" ;
        // alert('Please provide a first name.');
      } else if (lastName.value === "" || lastName.value === null) {
        alert('Please provide a last name.');
      } else if (!reg.test(password)) {
        alert('Please provide a password that includes at least one uppercase letter, one lowercase letter, one digit, and one special character. The password should be at least 8 characters long.');
      } else {
        alert('Success');
        window.location.href = 'index.html';
      }
    });
  });